#pragma once
#ifndef CPLAYER_H
#define CPLAYER_H
#include <iostream>
#include "CItem.h"

using namespace std;

class CPlayer
{
public:
	int iPlayerLevel = 1;
	int iPlayerHp = 10 * iPlayerLevel;
	int iPlayerAtaque = 2 * iPlayerLevel;
	int iPlayerDefensa = 2 * iPlayerLevel;
	int iPlayerExp = 0;
	int iPlayerGold;

	CPlayer();
	void ataque(int iAtaque);
	void defensa(int iDefensa);
	void sumarExp(int iExp);
	void sumarGold(int iGold);
	void restarGold(int iGold);
	virtual void statusPrint();
};

#endif // !CPLAYER_H