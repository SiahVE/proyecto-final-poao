#include "CPlayer.h"

CPlayer::CPlayer()
{

}

void CPlayer::sumarGold(int iGold)
{
	iPlayerGold = iPlayerGold + iGold;
}

void CPlayer::sumarExp(int iExp)
{
	iPlayerExp = iPlayerExp + iExp;
}

void CPlayer::ataque(int iAtaque)
{
	iPlayerAtaque = iPlayerAtaque + iAtaque;
}

void CPlayer::defensa(int iDefensa)
{
	iPlayerDefensa = iPlayerDefensa + iDefensa;
}

void CPlayer::restarGold(int iGold)
{
	iPlayerGold = iPlayerGold - iGold;
}

void CPlayer::statusPrint()
{
	cout << "Tu nivel es " << iPlayerLevel << "." << endl;
	cout << "Tu vida vida es " << iPlayerHp << "."<< endl;
	cout << "Tienes un ataque de " << iPlayerAtaque << "." << endl;
	cout << "Tienes una defensa de " << iPlayerDefensa << "." << endl;
	cout << "Tienes " << iPlayerExp << " de experiencia." << endl;
	cout << "Tienes " << iPlayerGold << " monedas de oro." << endl;
}