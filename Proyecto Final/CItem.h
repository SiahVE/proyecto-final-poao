#pragma once
#ifndef CITEM_H
#define CITEM_H
#include <iostream>

using namespace std;

class CItem
{
public:
	string strFinalItemName;
	string strFinalDescription;
	int iValue;

	CItem();
	void YoImprimo();
};

#endif // !CITEM_H
