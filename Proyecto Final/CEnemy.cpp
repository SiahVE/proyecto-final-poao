#include "CEnemy.h"

CEnemy::CEnemy()
{
	
}

CEnemy::CEnemy(int iID, int iNivel, int iGold)
{
	strEnemyName = "MuebleOAlgo";
	iEnemyLevel = iNivel;
	iEnemyHp = 2 * iEnemyLevel;
	iEnemyAttack = 2 * iEnemyLevel;
	iEnemyExp = 2 * iEnemyLevel;
	iEnemyGold = iGold;
	iEnemyId = iID;
}
