#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <ctime>
#include <conio.h>
#include "CWeapon.h"
#include "Carmor.h"
#include "CConsumables.h"
#include "CItem.h"
#include "CEnemy.h"
#include "CPlayer.h"
#define ENTER 13

using namespace std;

//String para el nombre del usuario.
string strPlayerName;
string strPlayerSaved;

//Archivo que guardara y leera la informacion del player.
ofstream fileSaveOutput;
ifstream fileSaveReader;
string sFileLine;
int iCont;
CPlayer player;

//Funciones a utilizar.
vector<string> randomEventGenerator(vector<string> &randomEventsRef, CPlayer& player);
void openInventory(vector<CItem>& inventoryRef);
void status();
void saveGame();

//Funcion que genera eventos aleatorios.
vector<string> randomEventGenerator(vector<string>& randomEventsRef, CPlayer &player)
{
	system("CLS");
	vector <string> combatMenu = { "===============================================================", "= = COMBAT MENU = =", " ", "1. ATTACK", "2. DEFEND", "3. ITEMS", "4. RUN", "===============================================================" };
	vector <string> ayudarAbuelita = { "Que deseas hacer: ", "1. Ayudar a la ancianita.", "2. Robarle sus monedas de oro." };
	bool bCombatMenuClose = true;
	bool bCombatOut = false;
	int iRandomEventPos;
	int min = 0;
	int max = randomEventsRef.size() - 1;
	int iEnemyMin = 1;
	int iEnemyMax = 20;
	int iEnemyMinGold = 1;
	int iEnemyMaxGold = 100;
	int iEnemyMaxExp = 50;
	int iEnemyMinExp = 5;
	int iEnemyMinID = 1;
	int iEnemyMaxID = 20;

	iRandomEventPos = rand() % (max - min + 1) + min;

	int iNivel = rand() % (iEnemyMax - iEnemyMin + 1) + iEnemyMin;
	int iGold = rand() % (iEnemyMaxGold - iEnemyMinGold + 1) + iEnemyMinGold;
	int iID = rand() % (iEnemyMaxID - iEnemyMinID + 1) + iEnemyMinID;
	int iExp = rand() % (iEnemyMaxExp - iEnemyMinExp + 1) + iEnemyMinExp;

	cout << randomEventsRef[iRandomEventPos] << endl;

	if (randomEventsRef[iRandomEventPos] == "Enemigo!!")
	{
		int iArrowPrint = 3;
		char cArrow = 'a';
		
		CEnemy Enemy1(iID, iNivel, iGold);

		while (bCombatMenuClose)
		{
			while (bCombatOut == false)
			{
				cArrow = 'm';

				while (cArrow != ENTER)
				{
					system("CLS");
					cout << "Juega vivo que es un " << Enemy1.strEnemyName << " de nivel " << Enemy1.iEnemyLevel << " con " << Enemy1.iEnemyHp << " de vida y creo que tiene " << Enemy1.iEnemyGold << " monedas." << endl;
					for (int i = 0; i < 8; i++)
					{
						if (iArrowPrint == i)
						{
							cout << " >" << combatMenu[i] << endl;
						}
						else
						{
							cout << "  " << combatMenu[i] << endl;
						}
					}

					cArrow = _getch();
					switch (cArrow)
					{
					case 's':
						if (iArrowPrint == 6)
						{
							iArrowPrint = 6;
						}
						else
						{
							iArrowPrint++;
						}
						break;
					case 'w':
						if (iArrowPrint == 3)
						{
							iArrowPrint = 3;
						}
						else
						{
							iArrowPrint--;
						}
						break;
					}
				}

				switch (iArrowPrint - 3)
				{
				case 0:
					player.ataque(10);
					Enemy1.iEnemyHp = Enemy1.iEnemyHp - player.iPlayerAtaque;
					player.iPlayerHp = player.iPlayerHp - Enemy1.iEnemyAttack;

					cout << "Le ocasionastes " << player.iPlayerAtaque << " al enemigo." << endl;
					cout << "Y recibistes " << Enemy1.iEnemyAttack << " de danio." << endl;

					if (Enemy1.iEnemyHp < 1)
					{
						cout << "Lo derrotastes !!" << endl;
						cout << "Recoge tu loot!!" << endl;
						cout << "Obtuvistes " << Enemy1.iEnemyGold << " monedas de oro y " << Enemy1.iEnemyExp << " de experiencia." << endl;
						player.sumarGold(Enemy1.iEnemyGold);
						player.sumarExp(Enemy1.iEnemyExp);
						bCombatOut = true;
					}

					if (player.iPlayerHp < 1)
					{
						cout << "Cringe que te hayan derrotado..." << endl;
						player.restarGold(iGold);
						cout << "Perdistes " << iGold << " monedas de oro." << endl;
						bCombatOut = true;
					}
					break;

				case 1:
					player.defensa(10);

					if (player.iPlayerDefensa > Enemy1.iEnemyAttack)
					{
						cout << "Te has defendido con exito!!" << endl;
						bCombatOut = true;
					}
					else
					{
						cout << "Tienes brazos de fideo cubrete bien." << endl;
						bCombatOut = true;
					}
					break;

				case 2:
					break;

				case 3:
					bCombatMenuClose = false;
					break;

				default:
					system("CLS");
					break;

				}
			}
		}
	}

	if (randomEventsRef[iRandomEventPos] == "Haz encontrado un cofre, que encontraras en el...")
	{
		int iGold = rand() % (iEnemyMaxGold - iEnemyMinGold + 1) + iEnemyMinGold;

		cout << "Wooow encontrastes " << iGold << " monedas de oro." << endl;

		player.sumarGold(iGold);
	}

	if (randomEventsRef[iRandomEventPos] == "Mientras descansabas un ladron te ha robado...")
	{
		int iGold = rand() % (iEnemyMaxGold - iEnemyMinGold + 1) + iEnemyMinGold;

		cout << "Ese rakataka me ha robado " << iGold << " monedas de oro." << endl;

		player.restarGold(iGold);
	}

	if (randomEventsRef[iRandomEventPos] == "Te haz topado con una dulce ancianita intentando cruzar una cueva tenebrosa con su bolso lleno de monedas de oro...")
	{
		int iArrowPrint = 1;
		char cArrow = 'a';

		while (cArrow != ENTER)
		{
			system("CLS");
			for (int i = 0; i < 3; i++)
			{
				if (iArrowPrint == i)
				{
					cout << " >" << ayudarAbuelita[i] << endl;
				}
				else
				{
					cout << "  " << ayudarAbuelita[i] << endl;
				}
			}

			cArrow = _getch();
			switch (cArrow)
			{
			case 's':
				if (iArrowPrint == 2)
				{
					iArrowPrint = 2;
				}
				else
				{
					iArrowPrint++;
				}
				break;
			case 'w':
				if (iArrowPrint == 1)
				{
					iArrowPrint = 1;
				}
				else
				{
					iArrowPrint--;
				}
				break;
			}

			switch (iArrowPrint - 1)
			{
			case 0:
				player.sumarExp(iExp);
				cout << "Por ser tan bueno la ancianita te bendijo y eso te dio " << iExp << " de experiencia" << endl;
				break;

			case 1: 
				player.sumarGold(iGold);
				cout << "Ni Judas se atrevio a tanto..." << endl;
				cout << "Pero conseguistes " << iGold << " monedas de oro." << endl;
				break;

			default:
				system("CLS");
				break;
			}
		}
	}
	system("pause");
	return randomEventsRef;
}

//Funcion que abrira el inventario del usuario.
void openInventory(vector<CItem>& inventoryRef)
{
	//Booleana para abrir el inventaro.
	bool inventoryOpened = true;

	while (inventoryOpened)
	{
		system("CLS");
		cout << "===============================================================" << endl;
		cout << "= = INVENTORY = =" << "\n" << endl;

		//Ciclo que itera sobre el inventario y lo imprime.
		for (int counter = 0; counter < inventoryRef.size(); counter++)
		{
			//cout << counter + 1 << ". " << inventoryRef[counter] << endl;
		}

		cout << inventoryRef.size() + 1 << ". " << "EXIT" << endl;
		cout << "===============================================================" << endl;
	}
}

//Funcion que muestra el status del jugador.
void status()
{
	player.statusPrint();
	system("PAUSE");
}

//Funcion que guarda el juego.
void saveGame()
{
	fileSaveOutput.open("playerData.txt", ios::out | ios::trunc);
	if (fileSaveOutput.is_open())
	{
		fileSaveOutput << strPlayerName << "\n";
		fileSaveOutput << "Level\n";
		fileSaveOutput << "Weapon\n";
		fileSaveOutput << "Armor\n";
		fileSaveOutput << "Inventory\n";
		fileSaveOutput.close();
		cout << "YOUR GAME HAS BEEN SAVED\n";
		system("pause");
	}
	getchar();
}

int main()
{
	//RANDOM SEED.
	srand(time(0));

	//Para futuros inventarios.
	vector<CItem> inventory;
	vector<CWeapon> weapons;
	vector<CArmor> armors;
	vector<CConsumables> consumables;

	//Vector para eventos mientras.
	vector<string> randomEvents = { "Haz encontrado un cofre, que encontraras en el...", "Mientras descansabas un ladron te ha robado...", "Haz viajado por horas y nada ha ocurrido...", "Enemigo!!", "Enemigo!!", "Te haz topado con una dulce ancianita intentando cruzar una cueva tenebrosa con su bolso lleno de monedas de oro..."};

	//Bool para mantener el menu abierto.
	bool bMenuOpen = true;

	//Player introduce su nombre, si existe el juego abrira su save.
	cout << "Enter your name: " << endl;
	cin >> strPlayerName;

	//Verificara si el nombre introducido es de un jugador anterior.
	fileSaveReader.open("playerData.txt");
	if (fileSaveReader.is_open())
	{
		iCont = 0;
		while (!fileSaveReader.eof())
		{
			getline(fileSaveReader, sFileLine);

			switch (iCont)
			{
			case 0: strPlayerSaved = sFileLine;
				break;
			}
			iCont++;
		}
		fileSaveReader.close();
	}

	//Si es el name de un jugador anterior el juego lo saludara y el jugador seguira con su save.
	if (strPlayerName == strPlayerSaved)
	{
		cout << "Welcome back " << strPlayerSaved << endl;
		system("pause");
	}

	//While que mantiene el menu abierto.
	while (bMenuOpen)
	{
		//Limpia la ventana de comandos. 
		system("CLS");

		//UI para el menu.
		vector<string> menu = { "===============================================================", "= = MAIN MENU = =", " ", "1. EXPLORE THE WILDERNESS", "2. FINAL BOSS (RECOMMENDED LEVEL: 20)", "3. STATUS", "4. INVENTORY", "5. SHOP", "6. SAVE GAME", "7. EXIT GAME", "===============================================================" };
		int iArrowPrint = 3;
		char cArrow = 'm';
		bool bOut = false;

		while (bOut == false)
		{
			//Impresion del menu.
			cArrow = 'm';

			while (cArrow != ENTER)
			{
				system("CLS");
				for (int i = 0; i < 11; i++)
				{
					if (iArrowPrint == i)
					{
						cout << " >" << menu[i] << endl;
					}
					else
					{
						cout << "  " << menu[i] << endl;
					}
				}

				//Flecha y movimiento W y S.
				cArrow = _getch();
				switch (cArrow)
				{
				case 's':
					if (iArrowPrint == 9)
					{
						iArrowPrint = 9;
					}
					else
					{
						iArrowPrint++;
					}
					break;
				case 'w':
					if (iArrowPrint == 3)
					{
						iArrowPrint = 3;
					}
					else
					{
						iArrowPrint--;
					}
					break;
				}
			}

			//Switch para el menu de entrada del usuario.
			switch (iArrowPrint - 3)
			{
			case 0:
				randomEventGenerator(randomEvents, player);
				break;

			case 1:

				break;

			case 2:
				status();
				break;

			case 3:
				openInventory(inventory);
				break;

			case 4:

				break;

			case 5:
				saveGame();
				break;

			case 6:
				bMenuOpen = false;
				bOut = true;
				break;

			default:
				system("CLS");
				break;
			}
		}
	} //Fin de while principal
	return 0;
}