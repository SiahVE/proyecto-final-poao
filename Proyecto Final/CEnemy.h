#pragma once
#ifndef CENEMY_H
#define CENEMY_H
#include <iostream>
#include "CItem.h"

using namespace std;

class CEnemy : public CItem
{
public:
	string strEnemyName;
	string strEnemySpecialAttack;
	int iEnemyHp;
	int iEnemyLevel;
	int iEnemyExp;
	int iEnemyGold;
	int iEnemyId;
	int iEnemyAttack;
	int iEnemyDefense;
	CItem Drops;

	CEnemy();
	CEnemy(int iID, int iNivel, int iGold);
};

#endif // !CENEMY_H
